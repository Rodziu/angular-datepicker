/*
 * Angular DatePicker & TimePicker plugin for AngularJS
 * Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */
!function(){
	'use strict';
	const datepickerModule = angular.module('angularDatePicker.datepicker', []);
	/**
	 * DatePicker Config
	 */
	datepickerModule.provider('DatePicker', function(){
		this.options = {
			minDate: undefined,
			maxDate: undefined,
			showIcon: true,
			hideOnPick: false,
			dayNames: angular.copy(Date.prototype.dayShortNames),
			monthNames: [],
			format: 'Y-m-d'
		};
		this.options.dayNames.push(this.options.dayNames.shift());
		for(let i = 0; i < 3; i++){
			let row = [];
			for(let j = 0; j < 4; j++){
				let number = (i * 4) + j + 1;
				row.push({
					name: Date.prototype.monthShortNames[number - 1],
					number: number
				});
			}
			this.options.monthNames.push(row);
			row = [];
		}
		// noinspection JSUnusedGlobalSymbols
		this.$get = function(){
			return this.options;
		};
	});
	/**
	 * Factory
	 */
	datepickerModule.factory('DatepickerFactory', [function(){
		return {
			isEnabledDate: function(ctrl, date, mode){
				if(!(date instanceof Date)){
					date = new Date(date + '');
				}
				const y = date.getFullYear(),
					m = date.getMonth(),
					d = date.getDate(),
					compare = function(compareMode){
						if(typeof ctrl[compareMode + 'Date'] !== 'undefined'){
							const cmpDate = ctrl[compareMode + 'Date'] instanceof Date
								? ctrl[compareMode + 'Date']
								: new Date(ctrl[compareMode + 'Date']),
								cmpFunction = function(a, b, equality){
									if(compareMode === 'min'){
										return a > b || (!!equality && a === b);
									}else{
										return a < b || (!!equality && a === b);
									}
								};
							if(cmpDate.isValid()){
								return cmpFunction(y, cmpDate.getFullYear())
									|| (
										y === cmpDate.getFullYear()
										&& (
											mode === 'year'
											|| cmpFunction(m, cmpDate.getMonth())
											|| (
												m === cmpDate.getMonth()
												&& (mode === 'month' || cmpFunction(d, cmpDate.getDate(), true))
											)
										)
									);
							}
						}
						return true;
					};
				const ret = compare('min') && compare('max');
				if(ret && typeof ctrl.disabledDates === 'function'){
					return ctrl.disabledDates(date, mode);
				}
				return ret;
			}
		};
	}]);
	/**
	 * @ngdoc component
	 * @name datepickerCalendar
	 *
	 * @param {expression} ngModel
	 * @param {Date|string} minDate
	 * @param {Date|string} maxDate
	 * @param {function} disabledDates
	 * @param {boolean} monthPicker
	 * @param {string} format
	 */
	datepickerModule.component('datepickerCalendar', {
		bindings: {
			ngModel: '=',
			minDate: '<?',
			maxDate: '<?',
			disabledDates: '<?'
		},
		templateUrl: 'src/templates/datepicker-calendar.ng',
		controllerAs: 'ctrl',
		/**
		 * @property ngChange
		 * @property {{}} datepicker
		 */
		require: {
			datepicker: '?^datepicker'
		},
		controller: ['$attrs', '$timeout', 'DatePicker', 'DatepickerFactory', function($attrs, $timeout, DatePicker, DatepickerFactory){
			let ctrl = this,
				lastRenderedDate;
			/**
			 * @type {Array}
			 */
			ctrl.displayData = [];
			/**
			 */
			ctrl.changeMode = function(mode){
				if(ctrl.displayMode !== mode){
					ctrl.displayMode = mode;
					ctrl.buildCalendar();
				}
			};
			/**
			 * @param {Date} date
			 * @param {String} mode
			 * @returns {boolean}
			 */
			ctrl.isEnabledDate = function(date, mode){
				return DatepickerFactory.isEnabledDate(ctrl, date, mode);
			};
			/**
			 * @param mode
			 * @returns {Date|boolean}
			 */
			ctrl.validDisplayAction = function(mode){
				let date = ctrl.currentDisplayDate.clone();
				switch(ctrl.displayMode){
					case 'days':
						date.sub(mode === 'prev' ? 1 : -1, 'month');
						return ctrl.isEnabledDate(date, 'month') ? date : false;
					case 'months':
						date.sub(mode === 'prev' ? 1 : -1, 'year');
						return ctrl.isEnabledDate(date, 'year') ? date : false;
					case 'years':
						let year = (Math.floor(ctrl.currentDisplayDate.getFullYear() / 12) * 12)
							+ (mode === 'prev' ? -1 : 12);
						if(ctrl.isEnabledDate(new Date(year + ''), 'year')){
							return date.sub(mode === 'prev' ? 12 : -12, 'year');
						}
						break;
				}
				return false;
			};
			/**
			 * @param {string} mode
			 */
			ctrl.displayAction = function(mode){
				let valid = ctrl.validDisplayAction(mode);
				if(valid){
					ctrl.currentDisplayDate = valid;
					ctrl.buildCalendar();
				}
			};
			/**
			 */
			ctrl.buildCalendar = function(){
				if(ctrl.displayMode === 'months'){
					return;
				}
				let i,
					row = [];
				if(ctrl.displayMode === 'days'){
					if(
						!(lastRenderedDate instanceof Date)
						|| ctrl.currentDisplayDate.format('Y-m') !== lastRenderedDate.format('Y-m')
					){
						ctrl.displayData = [];
						lastRenderedDate = ctrl.currentDisplayDate.clone();
						let firstDay = new Date(ctrl.currentDisplayDate.format('Y-m-01')),
							wd = firstDay.format('N') - 1;
						if(wd === 0){
							wd = 7;
						}
						firstDay.sub(wd, 'day');
						for(i = 1; i < 43; i++){
							row.push(firstDay.clone());
							if(i % 7 === 0){
								ctrl.displayData.push(row);
								row = [];
							}
							firstDay.add(1);
						}
					}
				}else{
					ctrl.displayData = [];
					let firstYear = Math.floor(ctrl.currentDisplayDate.getFullYear() / 12) * 12;
					for(i = 0; i < 3; i++){
						row = [];
						for(let j = 0; j < 4; j++){
							let year = firstYear + ((i * 4) + j);
							row.push(year);
						}
						ctrl.displayData.push(row);
						row = [];
					}
				}
			};
			/**
			 * @param date
			 * @param mode
			 */
			ctrl.pickDate = function(date, mode){
				if(!(date instanceof Date)){
					date = new Date(date + '');
				}
				if(!ctrl.isEnabledDate(date, mode)){
					return;
				}
				switch(mode){
					case 'day':
						ctrl.ngModel = date.format(ctrl.format);
						ctrl.currentDate = date;
						ctrl.currentDisplayDate = date;
						ctrl.buildCalendar();
						if(ctrl.datepicker !== null){
							if(typeof ctrl.datepicker.ngChange === 'function'){
								ctrl.datepicker.ngChange();
							}
							if(ctrl.datepicker.options.hideOnPick !== false){
								$timeout(function(){ // we need to defer it for ngModel to update properly
									ctrl.datepicker.isOpen = false;
								});
							}
						}
						break;
					case 'month':
						ctrl.currentDisplayDate.setMonth(date.getMonth());
						if(ctrl.monthPicker){
							ctrl.currentDisplayDate.setDate(1);
							ctrl.pickDate(ctrl.currentDisplayDate, 'day');
						}else{
							ctrl.changeMode('days');
						}
						break;
					case 'year':
						ctrl.currentDisplayDate.setFullYear(date.format('Y'));
						ctrl.changeMode('months');
						break;
				}
			};
			/**
			 */
			ctrl.$onInit = function(){
				ctrl.format = 'format' in $attrs
					? $attrs['format']
					: (ctrl.datepicker !== null && 'format' in ctrl.datepicker.$attrs
							? ctrl.datepicker.$attrs['format']
							: DatePicker['format']
					);
				ctrl.$doCheck();
				if(typeof ctrl.minDate === 'undefined'){
					ctrl.minDate = DatePicker.minDate;
				}
				if(typeof ctrl.maxDate === 'undefined'){
					ctrl.maxDate = DatePicker.maxDate;
				}
				ctrl.monthPicker = 'monthPicker' in $attrs
					|| (ctrl.datepicker !== null && 'monthPicker' in ctrl.datepicker.$attrs);
				ctrl.displayMode = ctrl.monthPicker ? 'months' : 'days';
				ctrl.dayNames = DatePicker.dayNames;
				ctrl.monthNames = DatePicker.monthNames;
				ctrl.buildCalendar();
			};
			/**
			 */
			ctrl.$doCheck = function(){
				let newDate;
				try{
					newDate = ctrl.ngModel instanceof Date
						? ctrl.ngModel.clone()
						: (new Date()).createFromFormat(ctrl.format, ctrl.ngModel);
				}catch(e){
					newDate = new Date();
				}
				if(
					newDate.isValid()
					&& (
						!(ctrl.currentDate instanceof Date)
						|| newDate.format('Y-m-d') !== ctrl.currentDate.format('Y-m-d')
					)
				){
					ctrl.currentDate = newDate;
					ctrl.currentDisplayDate = newDate.clone();
				}else if(typeof ctrl.currentDisplayDate === 'undefined'){
					newDate = new Date();
					ctrl.currentDate = newDate;
					ctrl.currentDisplayDate = newDate.clone();
				}
			};
		}]
	});
	/**
	 * @ngdoc component
	 * @name datepicker
	 *
	 * @param {expression} ngModel
	 * @param {Date|string} minDate
	 * @param {Date|string} maxDate
	 * @param {function} disabledDates
	 * @param {function} ngChange
	 * @param {boolean} ngDisabled
	 * @param {boolean} monthPicker
	 * @param {boolean} showIcon
	 * @param {boolean} hideOnPick
	 * @param {string} format
	 */
	datepickerModule.component('datepicker', {
		bindings: {
			ngModel: '=',
			minDate: '<?',
			maxDate: '<?',
			disabledDates: '<?',
			ngChange: '&?'
		},
		templateUrl: 'src/templates/datepicker.ng',
		/**
		 * @property dpCtrl
		 */
		controllerAs: 'dpCtrl',
		controller: [
			'$document', '$scope', '$element', '$attrs', 'DatePicker', 'DatepickerFactory',
			function($document, $scope, $element, $attrs, DatePicker, DatepickerFactory){
				let ctrl = this,
					onClick = function(e){
						if(ctrl.isOpen && !$element[0].contains(e.target)){
							ctrl.isOpen = false;
							$scope.$digest();
						}
					};
				ctrl.$attrs = $attrs;
				/**
				 */
				ctrl.$onInit = function(){
					ctrl.options = {};
					for(let d in DatePicker){
						if(DatePicker.hasOwnProperty(d)){
							if(typeof $attrs[d] !== 'undefined'){
								if($attrs[d] === 'false'){
									$attrs[d] = false;
								}else if($attrs[d] === 'true'){
									$attrs[d] = true;
								}
								ctrl.options[d] = $attrs[d];
							}else{
								ctrl.options[d] = DatePicker[d];
							}
						}
					}
					/**
					 * @type {boolean}
					 */
					ctrl.isOpen = false;
					/**
					 * @param date
					 * @param mode
					 * @returns {boolean}
					 */
					ctrl.isEnabledDate = function(date, mode){
						return DatepickerFactory.isEnabledDate(ctrl, date, mode);
					};
				};
				/**
				 */
				ctrl.$onChanges = function(){
					ctrl.isSmall = $element.hasClass('input-sm');
					ctrl.isLarge = $element.hasClass('input-lg');
				};
				/**
				 */
				ctrl.$onDestroy = function(){
					$document.off('click', onClick);
				};
				$document.on('click', onClick);
			}
		]
	});
	/**
	 * Datepicker input directive
	 */
	datepickerModule.directive('datepickerInput', ['DatePicker', function(DatePicker){
		const inputAttributes = ['required', 'disabled', 'readonly'];
		// noinspection JSUnusedGlobalSymbols
		return {
			restrict: 'A',
			require: ['ngModel', '^datepicker'],
			link: function(scope, element, attrs, ctrl){
				let ngModel = ctrl[0],
					datepicker = ctrl[1];
				for(let i = 0; i < inputAttributes.length; i++){
					(function(attribute){
						datepicker.$attrs.$observe(attribute, function(value){
							element.attr(attribute, value);
							if(attribute === 'disabled'){
								datepicker.isDisabled = value;
							}
						});
					})(inputAttributes[i]);
				}
				const validator = function(viewValue){
					if(
						typeof datepicker.isEnabledDate !== 'undefined'
						&& typeof viewValue !== 'undefined'
						&& viewValue !== ''
					){
						let date = (new Date()).createFromFormat(
							'format' in datepicker.$attrs ? datepicker.$attrs['format'] : DatePicker.format, viewValue
						);
						if(!date.isValid() || !datepicker.isEnabledDate(date, 'day')){
							ngModel.$setValidity('date', false);
							element[0].setCustomValidity(" ");
							return viewValue;
						}
					}
					ngModel.$setValidity('date', true);
					element[0].setCustomValidity("");
					return viewValue;
				};
				ngModel.$parsers.push(validator);
				ngModel.$formatters.push(validator);
			}
		};
	}]);
}();