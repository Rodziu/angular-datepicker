/*
 * Angular DatePicker & TimePicker plugin for AngularJS
 * Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */
angular.module('angularDatePicker', ['angularDatePicker.datepicker', 'angularDatePicker.timepicker']);