/*
 * Angular DatePicker & TimePicker plugin for AngularJS
 * Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */
!function(){
	'use strict';
	const timepickerModule = angular.module('angularDatePicker.timepicker', []);
	/**
	 * TimePicker Config
	 */
	timepickerModule.provider('TimePicker', function(){
		this.options = {
			pickHours: true,
			pickMinutes: true,
			pickSeconds: true,
			showIcon: true,
			hideOnPick: false,
			hours: [],
			minutes: []
		};
		const hours = [],
			minutes = [];
		let i, j;
		for(i = 0; i < 10; i++){
			const row = [],
				row2 = [];
			for(j = 0; j < 6; j++){
				if(i < 6 && j < 4){
					row.push({
						hour: Date.prototype.datePad((i * 4) + j)
					});
				}
				row2.push({
					minute: Date.prototype.datePad((i * 6) + j)
				});
			}
			hours.push(row);
			minutes.push(row2);
		}
		this.options.hours = hours;
		this.options.minutes = minutes;
		// noinspection JSUnusedGlobalSymbols
		this.$get = function(){
			return this.options;
		};
	});
	/**
	 * Timepicker picker
	 */
	timepickerModule.component('timepickerDrop', {
		bindings: {
			ngModel: '=',
			pickHours: '<?',
			pickMinutes: '<?',
			pickSeconds: '<?'
		},
		templateUrl: 'src/templates/timepicker-drop.ng',
		controllerAs: 'ctrl',
		/**
		 * @property ngChange
		 * @property {{}} timepicker
		 */
		require: {
			timepicker: '?^timepicker'
		},
		controller: ['$timeout', 'TimePicker', function($timeout, TimePicker){
			const ctrl = this,
				updateModel = function(){
					const val = [];
					if(ctrl.pickHours){
						val.push(Date.prototype.datePad(ctrl.hours));
					}
					if(ctrl.pickMinutes){
						val.push(Date.prototype.datePad(ctrl.minutes));
					}
					if(ctrl.pickSeconds){
						val.push(Date.prototype.datePad(ctrl.seconds));
					}
					ctrl.ngModel = val.join(":");
					if(ctrl.timepicker !== null){
						if(typeof ctrl.timepicker.ngChange === 'function'){
							ctrl.timepicker.ngChange();
						}
					}
				};
			/**
			 * @type {string}
			 */
			ctrl.mode = 'picker';
			/**
			 * @type {Array}
			 */
			ctrl.hoursArray = TimePicker.hours;
			/**
			 * @type {Array}
			 */
			ctrl.minutesArray = TimePicker.minutes;
			/**
			 * @type {number}
			 */
			ctrl.hours = 0;
			/**
			 * @type {number}
			 */
			ctrl.minutes = 0;
			/**
			 * @type {number}
			 */
			ctrl.seconds = 0;
			/**
			 * @param {string} mode
			 */
			ctrl.setMode = function(mode){
				ctrl.mode = mode;
			};
			/**
			 * @param {string} mode
			 * @param {boolean} [increment]
			 */
			ctrl.change = function(mode, increment){
				const limit = mode === 'hours' ? 23 : 59;
				if(!!increment){
					ctrl[mode]++;
				}else{
					ctrl[mode]--;
				}
				if(ctrl[mode] > limit){
					ctrl[mode] = 0;
				}else if(ctrl[mode] < 0){
					ctrl[mode] = limit;
				}
				updateModel();
			};
			/**
			 * @param {string} mode
			 * @param {string} value
			 */
			ctrl.pick = function(mode, value){
				ctrl[mode] = parseInt(value);
				ctrl.mode = 'picker';
				updateModel();
				if(ctrl.timepicker !== null && ctrl.timepicker.options.hideOnPick !== false){
					$timeout(function(){ // we need to defer it for ngModel to update properly
						ctrl.timepicker.isOpen = false;
					});
				}
			};
			/**
			 */
			ctrl.$onInit = function(){
				if(typeof ctrl.pickHours === 'undefined'){
					ctrl.pickHours = TimePicker.pickHours;
				}
				if(typeof ctrl.pickMinutes === 'undefined'){
					ctrl.pickMinutes = TimePicker.pickMinutes;
				}
				if(typeof ctrl.pickSeconds === 'undefined'){
					ctrl.pickSeconds = TimePicker.pickSeconds;
				}
				ctrl.$doCheck();
			};
			/**
			 */
			ctrl.$doCheck = function(){
				if(typeof ctrl.ngModel !== 'string'){

				}else{
					ctrl.hours = 0;
					ctrl.minutes = 0;
					ctrl.seconds = 0;
				}
				try{
					const val = ctrl.ngModel.split(':');
					let h = 0, m = 0, s = 0,
						hasM = false;
					loop:
						for(let v in val){
							if(val.hasOwnProperty(v)){
								switch(v){
									case '0':
										if(ctrl.pickHours){
											h = parseInt(val[v]);
										}else if(ctrl.pickMinutes){
											m = parseInt(val[v]);
											hasM = true;
										}else if(ctrl.pickSeconds){
											s = parseInt(val[v]);
											break loop;
										}
										break;
									case '1':
										if(ctrl.pickMinutes && !hasM){
											m = parseInt(val[v]);
										}else if(ctrl.pickSeconds){
											s = parseInt(val[v]);
											break loop;
										}
										break;
									case '2':
										if(ctrl.pickSeconds){
											s = parseInt(val[v]);
										}
										break loop;
								}
							}
						}
					if(!isNaN(h) && !isNaN(m) && !isNaN(s)){
						ctrl.hours = h;
						ctrl.minutes = m;
						ctrl.seconds = s;
					}
				}
				catch(e){
				}
			};
		}]
	});
	/**
	 * @ngdoc component
	 * @name timepicker
	 *
	 * @param {expression} ngModel
	 * @param {boolean} pickHours
	 * @param {boolean} pickMinutes
	 * @param {boolean} pickSeconds
	 * @param {function} ngChange
	 * @param {boolean} showIcon
	 * @param {boolean} hideOnPick
	 */
	timepickerModule.component('timepicker', {
		bindings: {
			ngModel: '=',
			pickHours: '<?',
			pickMinutes: '<?',
			pickSeconds: '<?',
			ngChange: '&?'
		},
		templateUrl: 'src/templates/timepicker.ng',
		/**
		 * @property tpCtrl
		 */
		controllerAs: 'tpCtrl',
		controller: [
			'$document', '$scope', '$element', '$attrs', 'TimePicker',
			function($document, $scope, $element, $attrs, TimePicker){
				let ctrl = this,
					onClick = function(e){
						if(ctrl.isOpen && !$element[0].contains(e.target)){
							ctrl.isOpen = false;
							$scope.$digest();
						}
					};
				/**
				 */
				ctrl.$onInit = function(){
					ctrl.options = {};
					for(let d in TimePicker){
						if(TimePicker.hasOwnProperty(d)){
							if(typeof $attrs[d] !== 'undefined'){
								if($attrs[d] === 'false'){
									$attrs[d] = false;
								}else if($attrs[d] === 'true'){
									$attrs[d] = true;
								}
								ctrl.options[d] = $attrs[d];
							}else{
								ctrl.options[d] = TimePicker[d];
							}
						}
					}
					/**
					 * @type {boolean}
					 */
					ctrl.isOpen = false;
					$attrs.$observe('required', function(value){
						ctrl.isRequired = value;
					});
				};
				/**
				 */
				ctrl.$onChanges = function(){
					ctrl.isSmall = $element.hasClass('input-sm');
					ctrl.isLarge = $element.hasClass('input-lg');
				};
				/**
				 */
				ctrl.$onDestroy = function(){
					$document.off('click', onClick);
				};
				$document.on('click', onClick);
			}
		]
	});
	/**
	 * datePad filter
	 */
	timepickerModule.filter('datePad', [function(){
		return function(input){
			if(typeof input === 'number'){
				return Date.prototype.datePad(input);
			}
			return input;
		};
	}]);
}();