# AngularJS Bootstrap Datepicker & Timepicker plugin

[Live Demo](https://mateuszrohde.pl/repository/angular-datepicker/demo/index.html)

## Prerequisites

- AngularJS,
- [date-extensions](https://yarnpkg.com/en/package/date-extensions),
- Twitter Bootstrap,
- Font Awesome.

## Installation

```
yarn add angularjs-bootstrap-datepicker
yarn install
```