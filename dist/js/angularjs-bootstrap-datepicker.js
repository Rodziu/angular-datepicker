/*
 * Angular DatePicker & TimePicker plugin for AngularJS
 * Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */
angular.module('angularDatePicker', ['angularDatePicker.datepicker', 'angularDatePicker.timepicker']);
/*
 * Angular DatePicker & TimePicker plugin for AngularJS
 * Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */
!function(){
	'use strict';
	const datepickerModule = angular.module('angularDatePicker.datepicker', []);
	/**
	 * DatePicker Config
	 */
	datepickerModule.provider('DatePicker', function(){
		this.options = {
			minDate: undefined,
			maxDate: undefined,
			showIcon: true,
			hideOnPick: false,
			dayNames: angular.copy(Date.prototype.dayShortNames),
			monthNames: [],
			format: 'Y-m-d'
		};
		this.options.dayNames.push(this.options.dayNames.shift());
		for(let i = 0; i < 3; i++){
			let row = [];
			for(let j = 0; j < 4; j++){
				let number = (i * 4) + j + 1;
				row.push({
					name: Date.prototype.monthShortNames[number - 1],
					number: number
				});
			}
			this.options.monthNames.push(row);
			row = [];
		}
		// noinspection JSUnusedGlobalSymbols
		this.$get = function(){
			return this.options;
		};
	});
	/**
	 * Factory
	 */
	datepickerModule.factory('DatepickerFactory', [function(){
		return {
			isEnabledDate: function(ctrl, date, mode){
				if(!(date instanceof Date)){
					date = new Date(date + '');
				}
				const y = date.getFullYear(),
					m = date.getMonth(),
					d = date.getDate(),
					compare = function(compareMode){
						if(typeof ctrl[compareMode + 'Date'] !== 'undefined'){
							const cmpDate = ctrl[compareMode + 'Date'] instanceof Date
								? ctrl[compareMode + 'Date']
								: new Date(ctrl[compareMode + 'Date']),
								cmpFunction = function(a, b, equality){
									if(compareMode === 'min'){
										return a > b || (!!equality && a === b);
									}else{
										return a < b || (!!equality && a === b);
									}
								};
							if(cmpDate.isValid()){
								return cmpFunction(y, cmpDate.getFullYear())
									|| (
										y === cmpDate.getFullYear()
										&& (
											mode === 'year'
											|| cmpFunction(m, cmpDate.getMonth())
											|| (
												m === cmpDate.getMonth()
												&& (mode === 'month' || cmpFunction(d, cmpDate.getDate(), true))
											)
										)
									);
							}
						}
						return true;
					};
				const ret = compare('min') && compare('max');
				if(ret && typeof ctrl.disabledDates === 'function'){
					return ctrl.disabledDates(date, mode);
				}
				return ret;
			}
		};
	}]);
	/**
	 * @ngdoc component
	 * @name datepickerCalendar
	 *
	 * @param {expression} ngModel
	 * @param {Date|string} minDate
	 * @param {Date|string} maxDate
	 * @param {function} disabledDates
	 * @param {boolean} monthPicker
	 * @param {string} format
	 */
	datepickerModule.component('datepickerCalendar', {
		bindings: {
			ngModel: '=',
			minDate: '<?',
			maxDate: '<?',
			disabledDates: '<?'
		},
		templateUrl: 'src/templates/datepicker-calendar.ng',
		controllerAs: 'ctrl',
		/**
		 * @property ngChange
		 * @property {{}} datepicker
		 */
		require: {
			datepicker: '?^datepicker'
		},
		controller: ['$attrs', '$timeout', 'DatePicker', 'DatepickerFactory', function($attrs, $timeout, DatePicker, DatepickerFactory){
			let ctrl = this,
				lastRenderedDate;
			/**
			 * @type {Array}
			 */
			ctrl.displayData = [];
			/**
			 */
			ctrl.changeMode = function(mode){
				if(ctrl.displayMode !== mode){
					ctrl.displayMode = mode;
					ctrl.buildCalendar();
				}
			};
			/**
			 * @param {Date} date
			 * @param {String} mode
			 * @returns {boolean}
			 */
			ctrl.isEnabledDate = function(date, mode){
				return DatepickerFactory.isEnabledDate(ctrl, date, mode);
			};
			/**
			 * @param mode
			 * @returns {Date|boolean}
			 */
			ctrl.validDisplayAction = function(mode){
				let date = ctrl.currentDisplayDate.clone();
				switch(ctrl.displayMode){
					case 'days':
						date.sub(mode === 'prev' ? 1 : -1, 'month');
						return ctrl.isEnabledDate(date, 'month') ? date : false;
					case 'months':
						date.sub(mode === 'prev' ? 1 : -1, 'year');
						return ctrl.isEnabledDate(date, 'year') ? date : false;
					case 'years':
						let year = (Math.floor(ctrl.currentDisplayDate.getFullYear() / 12) * 12)
							+ (mode === 'prev' ? -1 : 12);
						if(ctrl.isEnabledDate(new Date(year + ''), 'year')){
							return date.sub(mode === 'prev' ? 12 : -12, 'year');
						}
						break;
				}
				return false;
			};
			/**
			 * @param {string} mode
			 */
			ctrl.displayAction = function(mode){
				let valid = ctrl.validDisplayAction(mode);
				if(valid){
					ctrl.currentDisplayDate = valid;
					ctrl.buildCalendar();
				}
			};
			/**
			 */
			ctrl.buildCalendar = function(){
				if(ctrl.displayMode === 'months'){
					return;
				}
				let i,
					row = [];
				if(ctrl.displayMode === 'days'){
					if(
						!(lastRenderedDate instanceof Date)
						|| ctrl.currentDisplayDate.format('Y-m') !== lastRenderedDate.format('Y-m')
					){
						ctrl.displayData = [];
						lastRenderedDate = ctrl.currentDisplayDate.clone();
						let firstDay = new Date(ctrl.currentDisplayDate.format('Y-m-01')),
							wd = firstDay.format('N') - 1;
						if(wd === 0){
							wd = 7;
						}
						firstDay.sub(wd, 'day');
						for(i = 1; i < 43; i++){
							row.push(firstDay.clone());
							if(i % 7 === 0){
								ctrl.displayData.push(row);
								row = [];
							}
							firstDay.add(1);
						}
					}
				}else{
					ctrl.displayData = [];
					let firstYear = Math.floor(ctrl.currentDisplayDate.getFullYear() / 12) * 12;
					for(i = 0; i < 3; i++){
						row = [];
						for(let j = 0; j < 4; j++){
							let year = firstYear + ((i * 4) + j);
							row.push(year);
						}
						ctrl.displayData.push(row);
						row = [];
					}
				}
			};
			/**
			 * @param date
			 * @param mode
			 */
			ctrl.pickDate = function(date, mode){
				if(!(date instanceof Date)){
					date = new Date(date + '');
				}
				if(!ctrl.isEnabledDate(date, mode)){
					return;
				}
				switch(mode){
					case 'day':
						ctrl.ngModel = date.format(ctrl.format);
						ctrl.currentDate = date;
						ctrl.currentDisplayDate = date;
						ctrl.buildCalendar();
						if(ctrl.datepicker !== null){
							if(typeof ctrl.datepicker.ngChange === 'function'){
								ctrl.datepicker.ngChange();
							}
							if(ctrl.datepicker.options.hideOnPick !== false){
								$timeout(function(){ // we need to defer it for ngModel to update properly
									ctrl.datepicker.isOpen = false;
								});
							}
						}
						break;
					case 'month':
						ctrl.currentDisplayDate.setMonth(date.getMonth());
						if(ctrl.monthPicker){
							ctrl.currentDisplayDate.setDate(1);
							ctrl.pickDate(ctrl.currentDisplayDate, 'day');
						}else{
							ctrl.changeMode('days');
						}
						break;
					case 'year':
						ctrl.currentDisplayDate.setFullYear(date.format('Y'));
						ctrl.changeMode('months');
						break;
				}
			};
			/**
			 */
			ctrl.$onInit = function(){
				ctrl.format = 'format' in $attrs
					? $attrs['format']
					: (ctrl.datepicker !== null && 'format' in ctrl.datepicker.$attrs
							? ctrl.datepicker.$attrs['format']
							: DatePicker['format']
					);
				ctrl.$doCheck();
				if(typeof ctrl.minDate === 'undefined'){
					ctrl.minDate = DatePicker.minDate;
				}
				if(typeof ctrl.maxDate === 'undefined'){
					ctrl.maxDate = DatePicker.maxDate;
				}
				ctrl.monthPicker = 'monthPicker' in $attrs
					|| (ctrl.datepicker !== null && 'monthPicker' in ctrl.datepicker.$attrs);
				ctrl.displayMode = ctrl.monthPicker ? 'months' : 'days';
				ctrl.dayNames = DatePicker.dayNames;
				ctrl.monthNames = DatePicker.monthNames;
				ctrl.buildCalendar();
			};
			/**
			 */
			ctrl.$doCheck = function(){
				let newDate;
				try{
					newDate = ctrl.ngModel instanceof Date
						? ctrl.ngModel.clone()
						: (new Date()).createFromFormat(ctrl.format, ctrl.ngModel);
				}catch(e){
					newDate = new Date();
				}
				if(
					newDate.isValid()
					&& (
						!(ctrl.currentDate instanceof Date)
						|| newDate.format('Y-m-d') !== ctrl.currentDate.format('Y-m-d')
					)
				){
					ctrl.currentDate = newDate;
					ctrl.currentDisplayDate = newDate.clone();
				}else if(typeof ctrl.currentDisplayDate === 'undefined'){
					newDate = new Date();
					ctrl.currentDate = newDate;
					ctrl.currentDisplayDate = newDate.clone();
				}
			};
		}]
	});
	/**
	 * @ngdoc component
	 * @name datepicker
	 *
	 * @param {expression} ngModel
	 * @param {Date|string} minDate
	 * @param {Date|string} maxDate
	 * @param {function} disabledDates
	 * @param {function} ngChange
	 * @param {boolean} ngDisabled
	 * @param {boolean} monthPicker
	 * @param {boolean} showIcon
	 * @param {boolean} hideOnPick
	 * @param {string} format
	 */
	datepickerModule.component('datepicker', {
		bindings: {
			ngModel: '=',
			minDate: '<?',
			maxDate: '<?',
			disabledDates: '<?',
			ngChange: '&?'
		},
		templateUrl: 'src/templates/datepicker.ng',
		/**
		 * @property dpCtrl
		 */
		controllerAs: 'dpCtrl',
		controller: [
			'$document', '$scope', '$element', '$attrs', 'DatePicker', 'DatepickerFactory',
			function($document, $scope, $element, $attrs, DatePicker, DatepickerFactory){
				let ctrl = this,
					onClick = function(e){
						if(ctrl.isOpen && !$element[0].contains(e.target)){
							ctrl.isOpen = false;
							$scope.$digest();
						}
					};
				ctrl.$attrs = $attrs;
				/**
				 */
				ctrl.$onInit = function(){
					ctrl.options = {};
					for(let d in DatePicker){
						if(DatePicker.hasOwnProperty(d)){
							if(typeof $attrs[d] !== 'undefined'){
								if($attrs[d] === 'false'){
									$attrs[d] = false;
								}else if($attrs[d] === 'true'){
									$attrs[d] = true;
								}
								ctrl.options[d] = $attrs[d];
							}else{
								ctrl.options[d] = DatePicker[d];
							}
						}
					}
					/**
					 * @type {boolean}
					 */
					ctrl.isOpen = false;
					/**
					 * @param date
					 * @param mode
					 * @returns {boolean}
					 */
					ctrl.isEnabledDate = function(date, mode){
						return DatepickerFactory.isEnabledDate(ctrl, date, mode);
					};
				};
				/**
				 */
				ctrl.$onChanges = function(){
					ctrl.isSmall = $element.hasClass('input-sm');
					ctrl.isLarge = $element.hasClass('input-lg');
				};
				/**
				 */
				ctrl.$onDestroy = function(){
					$document.off('click', onClick);
				};
				$document.on('click', onClick);
			}
		]
	});
	/**
	 * Datepicker input directive
	 */
	datepickerModule.directive('datepickerInput', ['DatePicker', function(DatePicker){
		const inputAttributes = ['required', 'disabled', 'readonly'];
		// noinspection JSUnusedGlobalSymbols
		return {
			restrict: 'A',
			require: ['ngModel', '^datepicker'],
			link: function(scope, element, attrs, ctrl){
				let ngModel = ctrl[0],
					datepicker = ctrl[1];
				for(let i = 0; i < inputAttributes.length; i++){
					(function(attribute){
						datepicker.$attrs.$observe(attribute, function(value){
							element.attr(attribute, value);
							if(attribute === 'disabled'){
								datepicker.isDisabled = value;
							}
						});
					})(inputAttributes[i]);
				}
				const validator = function(viewValue){
					if(
						typeof datepicker.isEnabledDate !== 'undefined'
						&& typeof viewValue !== 'undefined'
						&& viewValue !== ''
					){
						let date = (new Date()).createFromFormat(
							'format' in datepicker.$attrs ? datepicker.$attrs['format'] : DatePicker.format, viewValue
						);
						if(!date.isValid() || !datepicker.isEnabledDate(date, 'day')){
							ngModel.$setValidity('date', false);
							element[0].setCustomValidity(" ");
							return viewValue;
						}
					}
					ngModel.$setValidity('date', true);
					element[0].setCustomValidity("");
					return viewValue;
				};
				ngModel.$parsers.push(validator);
				ngModel.$formatters.push(validator);
			}
		};
	}]);
}();
/*
 * Angular DatePicker & TimePicker plugin for AngularJS
 * Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */
!function(){
	'use strict';
	const timepickerModule = angular.module('angularDatePicker.timepicker', []);
	/**
	 * TimePicker Config
	 */
	timepickerModule.provider('TimePicker', function(){
		this.options = {
			pickHours: true,
			pickMinutes: true,
			pickSeconds: true,
			showIcon: true,
			hideOnPick: false,
			hours: [],
			minutes: []
		};
		const hours = [],
			minutes = [];
		let i, j;
		for(i = 0; i < 10; i++){
			const row = [],
				row2 = [];
			for(j = 0; j < 6; j++){
				if(i < 6 && j < 4){
					row.push({
						hour: Date.prototype.datePad((i * 4) + j)
					});
				}
				row2.push({
					minute: Date.prototype.datePad((i * 6) + j)
				});
			}
			hours.push(row);
			minutes.push(row2);
		}
		this.options.hours = hours;
		this.options.minutes = minutes;
		// noinspection JSUnusedGlobalSymbols
		this.$get = function(){
			return this.options;
		};
	});
	/**
	 * Timepicker picker
	 */
	timepickerModule.component('timepickerDrop', {
		bindings: {
			ngModel: '=',
			pickHours: '<?',
			pickMinutes: '<?',
			pickSeconds: '<?'
		},
		templateUrl: 'src/templates/timepicker-drop.ng',
		controllerAs: 'ctrl',
		/**
		 * @property ngChange
		 * @property {{}} timepicker
		 */
		require: {
			timepicker: '?^timepicker'
		},
		controller: ['$timeout', 'TimePicker', function($timeout, TimePicker){
			const ctrl = this,
				updateModel = function(){
					const val = [];
					if(ctrl.pickHours){
						val.push(Date.prototype.datePad(ctrl.hours));
					}
					if(ctrl.pickMinutes){
						val.push(Date.prototype.datePad(ctrl.minutes));
					}
					if(ctrl.pickSeconds){
						val.push(Date.prototype.datePad(ctrl.seconds));
					}
					ctrl.ngModel = val.join(":");
					if(ctrl.timepicker !== null){
						if(typeof ctrl.timepicker.ngChange === 'function'){
							ctrl.timepicker.ngChange();
						}
					}
				};
			/**
			 * @type {string}
			 */
			ctrl.mode = 'picker';
			/**
			 * @type {Array}
			 */
			ctrl.hoursArray = TimePicker.hours;
			/**
			 * @type {Array}
			 */
			ctrl.minutesArray = TimePicker.minutes;
			/**
			 * @type {number}
			 */
			ctrl.hours = 0;
			/**
			 * @type {number}
			 */
			ctrl.minutes = 0;
			/**
			 * @type {number}
			 */
			ctrl.seconds = 0;
			/**
			 * @param {string} mode
			 */
			ctrl.setMode = function(mode){
				ctrl.mode = mode;
			};
			/**
			 * @param {string} mode
			 * @param {boolean} [increment]
			 */
			ctrl.change = function(mode, increment){
				const limit = mode === 'hours' ? 23 : 59;
				if(!!increment){
					ctrl[mode]++;
				}else{
					ctrl[mode]--;
				}
				if(ctrl[mode] > limit){
					ctrl[mode] = 0;
				}else if(ctrl[mode] < 0){
					ctrl[mode] = limit;
				}
				updateModel();
			};
			/**
			 * @param {string} mode
			 * @param {string} value
			 */
			ctrl.pick = function(mode, value){
				ctrl[mode] = parseInt(value);
				ctrl.mode = 'picker';
				updateModel();
				if(ctrl.timepicker !== null && ctrl.timepicker.options.hideOnPick !== false){
					$timeout(function(){ // we need to defer it for ngModel to update properly
						ctrl.timepicker.isOpen = false;
					});
				}
			};
			/**
			 */
			ctrl.$onInit = function(){
				if(typeof ctrl.pickHours === 'undefined'){
					ctrl.pickHours = TimePicker.pickHours;
				}
				if(typeof ctrl.pickMinutes === 'undefined'){
					ctrl.pickMinutes = TimePicker.pickMinutes;
				}
				if(typeof ctrl.pickSeconds === 'undefined'){
					ctrl.pickSeconds = TimePicker.pickSeconds;
				}
				ctrl.$doCheck();
			};
			/**
			 */
			ctrl.$doCheck = function(){
				if(typeof ctrl.ngModel !== 'string'){

				}else{
					ctrl.hours = 0;
					ctrl.minutes = 0;
					ctrl.seconds = 0;
				}
				try{
					const val = ctrl.ngModel.split(':');
					let h = 0, m = 0, s = 0,
						hasM = false;
					loop:
						for(let v in val){
							if(val.hasOwnProperty(v)){
								switch(v){
									case '0':
										if(ctrl.pickHours){
											h = parseInt(val[v]);
										}else if(ctrl.pickMinutes){
											m = parseInt(val[v]);
											hasM = true;
										}else if(ctrl.pickSeconds){
											s = parseInt(val[v]);
											break loop;
										}
										break;
									case '1':
										if(ctrl.pickMinutes && !hasM){
											m = parseInt(val[v]);
										}else if(ctrl.pickSeconds){
											s = parseInt(val[v]);
											break loop;
										}
										break;
									case '2':
										if(ctrl.pickSeconds){
											s = parseInt(val[v]);
										}
										break loop;
								}
							}
						}
					if(!isNaN(h) && !isNaN(m) && !isNaN(s)){
						ctrl.hours = h;
						ctrl.minutes = m;
						ctrl.seconds = s;
					}
				}
				catch(e){
				}
			};
		}]
	});
	/**
	 * @ngdoc component
	 * @name timepicker
	 *
	 * @param {expression} ngModel
	 * @param {boolean} pickHours
	 * @param {boolean} pickMinutes
	 * @param {boolean} pickSeconds
	 * @param {function} ngChange
	 * @param {boolean} showIcon
	 * @param {boolean} hideOnPick
	 */
	timepickerModule.component('timepicker', {
		bindings: {
			ngModel: '=',
			pickHours: '<?',
			pickMinutes: '<?',
			pickSeconds: '<?',
			ngChange: '&?'
		},
		templateUrl: 'src/templates/timepicker.ng',
		/**
		 * @property tpCtrl
		 */
		controllerAs: 'tpCtrl',
		controller: [
			'$document', '$scope', '$element', '$attrs', 'TimePicker',
			function($document, $scope, $element, $attrs, TimePicker){
				let ctrl = this,
					onClick = function(e){
						if(ctrl.isOpen && !$element[0].contains(e.target)){
							ctrl.isOpen = false;
							$scope.$digest();
						}
					};
				/**
				 */
				ctrl.$onInit = function(){
					ctrl.options = {};
					for(let d in TimePicker){
						if(TimePicker.hasOwnProperty(d)){
							if(typeof $attrs[d] !== 'undefined'){
								if($attrs[d] === 'false'){
									$attrs[d] = false;
								}else if($attrs[d] === 'true'){
									$attrs[d] = true;
								}
								ctrl.options[d] = $attrs[d];
							}else{
								ctrl.options[d] = TimePicker[d];
							}
						}
					}
					/**
					 * @type {boolean}
					 */
					ctrl.isOpen = false;
					$attrs.$observe('required', function(value){
						ctrl.isRequired = value;
					});
				};
				/**
				 */
				ctrl.$onChanges = function(){
					ctrl.isSmall = $element.hasClass('input-sm');
					ctrl.isLarge = $element.hasClass('input-lg');
				};
				/**
				 */
				ctrl.$onDestroy = function(){
					$document.off('click', onClick);
				};
				$document.on('click', onClick);
			}
		]
	});
	/**
	 * datePad filter
	 */
	timepickerModule.filter('datePad', [function(){
		return function(input){
			if(typeof input === 'number'){
				return Date.prototype.datePad(input);
			}
			return input;
		};
	}]);
}();angular.module('angularDatePicker').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('src/templates/datepicker-calendar.ng',
    "<table class=\"table table-condensed table-datepicker\"><thead><tr><th ng-click=\"ctrl.displayAction('prev')\" ng-class=\"{'disabled': !ctrl.validDisplayAction('prev')}\"><i class=\"fa fa-chevron-left\"></i></th><th ng-click=\"ctrl.changeMode(ctrl.displayMode === 'days' ? 'months' : 'years')\" colspan=\"{{ctrl.displayMode == 'days' ? 5 : 2}}\" ng-switch=ctrl.displayMode><span ng-switch-when=days>{{ctrl.currentDisplayDate.format('F Y')}}</span> <span ng-switch-when=months>{{ctrl.currentDisplayDate.format('Y')}}</span> <span ng-switch-when=years>{{ctrl.displayData[0][0]}} - {{ctrl.displayData[2][3]}}</span></th><th ng-click=\"ctrl.displayAction('next')\" ng-class=\"{'disabled': !ctrl.validDisplayAction('next')}\"><i class=\"fa fa-chevron-right\"></i></th></tr><tr ng-show=\"ctrl.displayMode == 'days'\"><th ng-repeat=\"d in ::ctrl.dayNames\">{{d}}</th></tr></thead><tbody ng-switch=ctrl.displayMode><tr ng-switch-when=days ng-repeat=\"row in ctrl.displayData\"><td ng-repeat=\"d in ::row\" ng-click=\"ctrl.pickDate(d, 'day')\" ng-class=\"{\n" +
    "				'old': d.format('Y-m') < ctrl.currentDisplayDate.format('Y-m'),\n" +
    "				'fut': d.format('Y-m') > ctrl.currentDisplayDate.format('Y-m'),\n" +
    "				'active': d.format('Ymd') == ctrl.currentDate.format('Ymd'),\n" +
    "				'disabled': !ctrl.isEnabledDate(d, 'day')}\">{{::d.format('j')}}</td></tr><tr ng-switch-when=months ng-repeat=\"row in ::ctrl.monthNames\" class=months><td ng-repeat=\"m in ::row\" ng-click=\"ctrl.pickDate(ctrl.currentDisplayDate.format('Y-' + m.number), 'month')\" ng-class=\"{'active': ctrl.currentDisplayDate.format('Y' + m.number) == ctrl.currentDate.format('Yn'),\n" +
    "				'disabled': !ctrl.isEnabledDate(ctrl.currentDisplayDate.format('Y-' + m.number), 'month')}\">{{::m.name}}</td></tr><tr ng-switch-when=years ng-repeat=\"row in ctrl.displayData\" class=years><td ng-repeat=\"y in ::row\" ng-click=\"ctrl.pickDate(y, 'year')\" ng-class=\"{'active': y == ctrl.currentDate.getFullYear(), 'disabled': !ctrl.isEnabledDate(y + '', 'year')}\">{{::y}}</td></tr></tbody></table>"
  );


  $templateCache.put('src/templates/datepicker.ng',
    "<div class=dropdown ng-class=\"{\n" +
    "	 'input-group': dpCtrl.options.showIcon, 'input-group-sm': dpCtrl.isSmall, 'input-group-lg': dpCtrl.isLarge,\n" +
    "	 'open': dpCtrl.isOpen}\"><input class=form-control ng-model=dpCtrl.ngModel datepicker-input ng-click=\"dpCtrl.isOpen = true\"><ul class=\"dropdown-menu dropdown-menu-right angular-datepicker\" ng-click=$event.stopPropagation()><li ng-if=dpCtrl.isOpen><datepicker-calendar ng-model=dpCtrl.ngModel min-date=dpCtrl.minDate max-date=dpCtrl.maxDate disabled-dates=dpCtrl.disabledDates></datepicker-calendar></li></ul><span class=input-group-btn ng-show=::dpCtrl.options.showIcon><button type=button class=\"btn btn-default\" data-ng-disabled=dpCtrl.isDisabled ng-click=\"dpCtrl.isOpen = true\"><i class=\"fa fa-calendar\"></i></button></span></div>"
  );


  $templateCache.put('src/templates/timepicker-drop.ng',
    "<table class=\"table table-condensed table-timepicker\" ng-switch=ctrl.mode><tbody ng-switch-when=picker><tr><td ng-if=ctrl.pickHours><a ng-click=\"ctrl.change('hours', true)\"><i class=\"fa fa-chevron-up\"></i></a></td><td ng-if=ctrl.pickMinutes><a ng-click=\"ctrl.change('minutes', true)\"><i class=\"fa fa-chevron-up\"></i></a></td><td ng-if=ctrl.pickSeconds><a ng-click=\"ctrl.change('seconds', true)\"><i class=\"fa fa-chevron-up\"></i></a></td></tr><tr class=timepicker-values><td ng-if=ctrl.pickHours><a ng-click=\"ctrl.setMode('hours')\">{{ctrl.hours | datePad}}</a></td><td ng-if=ctrl.pickMinutes><a ng-click=\"ctrl.setMode('minutes')\">{{ctrl.minutes | datePad}}</a></td><td ng-if=ctrl.pickSeconds><a ng-click=\"ctrl.setMode('seconds')\">{{ctrl.seconds | datePad}}</a></td></tr><tr><td ng-if=ctrl.pickHours><a ng-click=\"ctrl.change('hours')\"><i class=\"fa fa-chevron-down\"></i></a></td><td ng-if=ctrl.pickMinutes><a ng-click=\"ctrl.change('minutes')\"><i class=\"fa fa-chevron-down\"></i></a></td><td ng-if=ctrl.pickSeconds><a ng-click=\"ctrl.change('seconds')\"><i class=\"fa fa-chevron-down\"></i></a></td></tr></tbody><tbody ng-switch-when=hours><tr ng-repeat=\"h in ::ctrl.hoursArray\" class=hours><td ng-repeat=\"hh in ::h\"><a ng-click=\"ctrl.pick('hours', hh.hour)\">{{::hh.hour}}</a></td></tr></tbody><tbody ng-switch-default><tr ng-repeat=\"m in ::ctrl.minutesArray\" class=hours><td ng-repeat=\"mm in ::m\"><a ng-click=\"ctrl.pick(ctrl.mode, mm.minute)\">{{::mm.minute}}</a></td></tr></tbody></table>"
  );


  $templateCache.put('src/templates/timepicker.ng',
    "<div class=dropdown ng-class=\"{\n" +
    "	 'input-group': tpCtrl.options.showIcon, 'input-group-sm': tpCtrl.isSmall, 'input-group-lg': tpCtrl.isLarge,\n" +
    "	 'open': tpCtrl.isOpen}\"><input class=form-control ng-model=tpCtrl.ngModel ng-required=tpCtrl.isRequired ng-click=\"tpCtrl.isOpen = true\" readonly><ul class=\"dropdown-menu dropdown-menu-right angular-timepicker\" ng-click=$event.stopPropagation()><li ng-if=tpCtrl.isOpen><timepicker-drop ng-model=tpCtrl.ngModel pick-hours=tpCtrl.pickHours pick-minutes=tpCtrl.pickMinutes pick-seconds=tpCtrl.pickSeconds></timepicker-drop></li></ul><span class=input-group-btn ng-show=::tpCtrl.options.showIcon><button type=button class=\"btn btn-default\" data-ng-disabled=tpCtrl.isDisabled ng-click=\"tpCtrl.isOpen = true\"><i class=\"fa fa-clock-o\"></i></button></span></div>"
  );

}]);
