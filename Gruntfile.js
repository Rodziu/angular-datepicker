/*
 * Angular DatePicker & TimePicker plugin for AngularJS
 * Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */
module.exports = function(grunt){
	const banner = '/*! <%= pkg.description %> v.<%= pkg.version %> | (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %> */';
	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		copy: {
			main: {
				files: [
					{expand: true, cwd: 'src/css', src: '**', dest: 'dist/css'}
				]
			}
		},
		concat: {
			dist: {
				src: [
					'src/js/**.js'
				],
				dest: 'dist/js/<%= pkg.name %>.js'
			}
		},
		ngtemplates: {
			app: {
				src: 'src/templates/**.ng',
				dest: 'dist/js/<%= pkg.name %>.js',
				options: {
					append: true,
					module: 'angularDatePicker',
					htmlmin: {
						collapseWhitespace: true,
						collapseBooleanAttributes: true,
						removeAttributeQuotes: true,
						removeComments: true,
						removeEmptyAttributes: true,
						removeRedundantAttributes: true,
						removeScriptTypeAttributes: true,
						removeStyleLinkTypeAttributes: true
					}
				}
			}
		},
		uglify: {
			options: {
				banner: banner
			},
			build: {
				src: 'dist/js/<%= pkg.name %>.js',
				dest: 'dist/js/<%= pkg.name %>.min.js'
			}
		},
		cssmin: {
			target: {
				files: [{
					expand: true,
					cwd: 'dist/css',
					src: ['*.css', '!*.min.css'],
					dest: 'dist/css',
					ext: '.min.css'
				}]
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-angular-templates');
	grunt.loadNpmTasks('grunt-contrib-uglify-es');
	grunt.loadNpmTasks('grunt-contrib-cssmin');

	grunt.registerTask('default', ['copy', 'concat', 'ngtemplates', 'uglify', 'cssmin']);

};